//Amanda Morrison
//2-15-19
//CSE 02 section 210

// This program randomly selects a card from a deck of cards
// I will generate a random number from 1 to 52

import java.lang.Math; // importing the math class

public class CardGenerator{
  // open class
  public static void main (String[] args) {
    //main method that you need for every java program 
    int myRandom=(int)(Math.random()*52)+1; // create a random number with decimals between 0 and 1 not including 1. then multiply by 52 to have the range between 0 and 51, then cast to integer to make everything a whole number. then add one to have the range between 1 and 52
    
    if (myRandom <=13){ //if the card is less than 13 it is a diamond
      int number= myRandom%13; // find what number within the suit
        if (number == 11){ //an eleven is a jack
          System.out.println ("You picked the Jack of Diamonds"); //print statement
        }
          else if(number == 12){ // a 12 is a queen
          System.out.println ("You picked the Queen of Diamonds"); //print statement
        }
      else if(number == 0){ // a 13 is a king
          System.out.println ("You picked the King of Diamonds"); //print statement
        }
      else if(number == 1){ // a 1 is an ace
          System.out.println ("You picked the Ace of Diamonds"); //print statement
        }
      else { // if not one of the numbers above
        System.out.println("You picked the "+ number + " of Diamonds"); // print statement
      }
    }
    
    if (myRandom >=14 && myRandom <=26){ //if the card is between these it is a club
      int number= myRandom%13; // find what number within the suit
        if (number == 11){ //an eleven is a jack
          System.out.println ("You picked the Jack of Clubs"); //print statement
        }
          else if(number == 12){ // a 12 is a queen
          System.out.println ("You picked the Queen of Clubs"); //print statement
        }
      else if(number == 0){ // a 0 is a king
          System.out.println ("You picked the King of Clubs"); //print statement
        }
      else if(number == 1){ // a 1 is an ace
          System.out.println ("You picked the Ace of Clubs"); //print statement
        }
      else { // if not one of the numbers above
        System.out.println("You picked the "+ number + " of Clubs"); // print statement
      }
    }
    
    if (myRandom >=27 && myRandom<=39){ //if the card is between the values it is a Heart
      int number= myRandom%13; // find what number within the suit
        if (number == 11){ //an eleven is a jack
          System.out.println ("You picked the Jack of Hearts"); //print statement
        }
          else if(number == 12){ // a 12 is a queen
          System.out.println ("You picked the Queen of Hearts"); //print statement
        }
      else if(number == 0){ // a 0 is a king
          System.out.println ("You picked the King of Hearts"); //print statement
        }
      else if(number == 1){ // a 1 is an ace
          System.out.println ("You picked the Ace of Hearts"); //print statement
        }
      else { // if not one of the numbers above
        System.out.println("You picked the "+ number + " of Hearts"); // print statement
      }
    }
    
    if (myRandom > 40){ //if the card is greater than 40 it is a Spade
      int number= myRandom%13; // find what number within the suit
        if (number == 11){ //an eleven is a jack
          System.out.println ("You picked the Jack of Spades"); //print statement
        }
          else if(number == 12){ // a 12 is a queen
          System.out.println ("You picked the Queen of Spades"); //print statement
        }
      else if(number == 0){ // a 13 is a king
          System.out.println ("You picked the King of Spades"); //print statement
        }
      else if(number == 1){ // a 1 is an ace
          System.out.println ("You picked the Ace of Spades"); //print statement
        }
      else { // if not one of the numbers above
        System.out.println("You picked the "+ number + " of Spades"); // print statement
      }
    }
    
    
  } // end of main method 
} //end of class