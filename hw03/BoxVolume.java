//Amanda Morrison
//2-8-19
//CSE 02 section 210

// This program finds the volume inside the box
// I will take input from the user and perform calculations with inputs

import java.util.Scanner; // import a scanner

public class BoxVolume{
  // open class
  public static void main (String[] args) {
    //main method that you need for every java program 
    Scanner myScanner = new Scanner(System.in); // making the instance of scanner declared which will take input
    System.out.print ("Enter the width side of the box: "); //prompt to enter the width and will go to the beginning of the next line
    double width= myScanner.nextDouble(); // store the user input into the variable and send to the next line
    System.out.print ("Enter the length of the box: "); //prompt to enter the length and will go to the beginning of the next line
    double length= myScanner.nextDouble(); // store the user input into the variable and send to the next line
    System.out.print ("Enter the height of the box: "); //prompt to enter the height and will go to the beginning of the next line
    double height= myScanner.nextDouble(); // store the user input into the variable and send to the next line
    double volume = width*height*length; // calculate the volume and store it into the variable 
   
    int widthf= (int)width; //cast width into an int
    int lengthf= (int)length; //cast length into an int
    int heightf= (int)height; //cast height into an int
    int volumef= (int)volume; //cast volume into an int
    
    System.out.println ("The width side of the box is: "+ widthf); // print out the width of the box
    System.out.println ("The length of the box is: "+ lengthf); //print out the length of the box
    System.out.println ("The height of the box is: "+ heightf); //print out the height of the box
    System.out.println ("     "); //new paragraph 
    System.out.println ("The volume inside the box is: "+ volumef); // print out the volume of the box
    
     } // end of main method
  } //end of class