//Amanda Morrison
//2-8-19
//CSE 02 section 210

// This program converts meters to inches
// I will take input from the user and perform calculations with inputs

import java.util.Scanner; // import a scanner

public class Convert{
  // open class
  public static void main (String[] args) {
    //main method that you need for every java program 
    
    Scanner myScanner = new Scanner(System.in); // making the instance of scanner declared which will take input
    System.out.print ("Enter the distance in meters: "); //prompt to enter the distance and will go to the beginning of the next line
    double metersDistance= myScanner.nextDouble(); // store the user input into the variable and send to the next line
    double conversion= 39.370079; //there are 39.37007874 inches in a meter
    double inches= metersDistance * conversion ; //calculate the inches in a the meters entered
    
    //ensure 4 decimal places for the inches 
    inches= inches*10000; // move decimal four places to the right
    int inchesDistance= (int) inches; //convert into an int so no decimals
    double inchesf= (double) inchesDistance/10000; //convert back into a double while dividing by 10000 and moving the decimals 4 to the left
    
    
    System.out.println (metersDistance + " meters is " + inchesf + " inches."); // print statement to display results
     } // end of main method
  } //end of class