//Amanda Morrison
//4-5-19
//CSE 02 section 210 hw08

// This program uses methods and arrays
// I will generate a random array of chars and sort them

import java.util.Arrays; //import array
import java.util.Random; //import a random generator

public class Letters{ // open class
  
  public static char [] getAtoM (int times, char [] array){ //get A to M method that takes in times and array
    char [] AtoM = new char [times]; //create an empty array that is times in length
    int b = 0; //declare and itialize b
    for (int i=0; i<times; i++){ //for each num in array
          int a = array[i]; //set a to the value within the array
          if ((char)a >64 && (char)a <78 || (char)a > 96 && (char)a <110){ //cast a to a char and see if it is between a to m for both upper and lower case
            AtoM[b] = (char)a; //populate the array at position b with the char
            b++; //incriment b
          }
        }
    return AtoM; //return new array
    
  }
  
  public static char [] getNtoZ (int times, char [] array){ //get N to Z method that takes in times and array
    char [] NtoZ = new char [times]; // create an empty array that is times in length
    int b = 0; //declare and itialize b
    for (int i=0; i<times; i++){ //for each num in array
      int a = array[i]; // set a to the value within the array
      if ((char)a >77 && (char)a <91 || (char)a > 109 && (char)a <123){ //cast a to a char and see if it is between n and z for both upper and lower case
        NtoZ[b] = (char)a; // populate the array at position b with the char
        b++; //inciment b
      }
    }
    return NtoZ; //return new array 
  }
  
public static void main (String[] args) { 
    //main method that you need for every java program 
Random randomGenerator = new Random(); //making the instance of random declaration
int a = 0; //declare and initialize
int b = 0; //declare and initialize
  
  int times = randomGenerator.nextInt(30)+1; //random number between 1 and 30
  char [] array; //initialize array
  array = new char [times]; //set array length to times
 
  
  System.out.print ("Random character Array: "); //print statement
  for (int i = 0; i <times; i++){ //for each value in the array
	 a = randomGenerator.nextInt (52)+1; //generate random int from 1 to 52 for each letter in the alphabet upper and lower case
   if (a<27){ //if the int is less than 27
     b = 64 + a; //set b to the number plus 64 to get a char from A to Z
   }
    else { // if a is over 26
      a = a-26; //reset to make a an int from 1 to 26, these will be for lowercase
      b = 96 + a; //set b to the new value of a plus 96 to get a char from a to z
    }
    array[i] = ((char)b); //populate the array as a char 
    System.out.print (array[i] + " "); //print out array value
}
  System.out.println(); //new line
  
  char[] AtoM = getAtoM(times, array); //call method and create new char
  char[] NtoZ = getNtoZ(times, array); //call method and create new char
  
  System.out.print ("AtoM characters: "); //print statement
  for (int j = 0; j<times; j++){ // for each num in the array
    System.out.print (AtoM[j] + " "); //print out the char at that location
  }
  System.out.println(); //new line
  
  System.out.print ("NtoZ characters: "); //print statement
    for (int k = 0; k<times; k++){ //for each num in the array
    System.out.print (NtoZ[k] + " "); //print out the char at that location
  }
  System.out.println(); //new line
  
   } // end of main method
} //end of class