//Amanda Morrison
//4-5-19
//CSE 02 section 210 hw08

// This program uses methods and arrays
// I will compare array inputs to with a random array

import java.util.Arrays; //import array
import java.util.Random; //import a random generator
import java.util.Scanner; //import a scanner

public class PlayLottery{ // open class
  
  public static boolean userWins (int [] user, int [] winning){ //get A to M method that takes in times and array
   boolean win = false;
    if (user[1] == winning[1] && user[2] == winning[2] && user[3] == winning[3] && user[4] == winning[4] && user[0] == winning[0]){
      win = true;
    }
    return win; //return new array
  }
  
  public static int [] numbersPicked (){ //get N to Z method that takes in times and array
    Random randomGenerator = new Random(); //making the instance of random declaration
    int [] nums = new int [5]; // create an empty array that is times in length
    for (int i=0; i<5; i++){ //for each num in array
      nums [i] = randomGenerator.nextInt (60);
      }
    
    while(nums[0]== nums[1] || nums[0]== nums[2] || nums[0]== nums[3] || nums[0]== nums[4] ){
      nums [0] = randomGenerator.nextInt (60);
    }
    while(nums[1]== nums[2] || nums[1]== nums[3] || nums[1]== nums[4] ){
      nums [1] = randomGenerator.nextInt (60);
    }
    while(nums[2]== nums[3] || nums[2]== nums[4] ){
      nums [2] = randomGenerator.nextInt (60);
    }
    while(nums[3]== nums[4] ){
      nums [3] = randomGenerator.nextInt (60);
    }
    
    for (int i=0; i<4; i++){ //for each num in array
      System.out.print (nums [i] + ", ");
      }
    System.out.print (nums[4]);
    return nums; //return new array 
  }
  
public static void main (String[] args) { 
    //main method that you need for every java program 
Scanner myScanner = new Scanner(System.in); // making the instance of the scanner
  int one = -1;
  int two = -1;
  int three = -1;
  int four = -1; 
  int five = -1;
    
  System.out.print ("Enter a number between 0 and 59: "); //print statement
  while (one<0){
    if (myScanner.hasNextInt()){ // if it is an int
          one= myScanner.nextInt(); // set int to num
            if (one>=0 && one <60){ // if num is between 1 and 10
              break; //leave loop
            }
         one=-1; //reset num
      }
      else { // if not an int
        String junkWord = myScanner.next(); // discard input 
      }
      System.out.println ("Error: enter an int between 0 and 59"); //prompt to enter a positive integer
    }
    
  System.out.print ("Enter a number between 0 and 59: "); //print statement
  while (two<0){
    if (myScanner.hasNextInt()){ // if it is an int
          two= myScanner.nextInt(); // set int to num
            if (two>=0 && two <60 && two!=one){ // if num is between 1 and 10
              break; //leave loop
            }
         two=-1; //reset num
      }
      else { // if not an int
        String junkWord2 = myScanner.next(); // discard input 
      }
      System.out.println ("Error: enter an int between 0 and 59 with no duplicates"); //prompt to enter a positive integer
    }
  
  System.out.print ("Enter a number between 0 and 59: "); //print statement
  while (three<0){
    if (myScanner.hasNextInt()){ // if it is an int
          three= myScanner.nextInt(); // set int to num
            if (three>=0 && three <60 && three!=two && three!=one){ // if num is between 1 and 10
              break; //leave loop
            }
         three=-1; //reset num
      }
      else { // if not an int
        String junkWord3 = myScanner.next(); // discard input 
      }
      System.out.println ("Error: enter an int between 0 and 59 with no duplicates"); //prompt to enter a positive integer
    }
  
  System.out.print ("Enter a number between 0 and 59: "); //print statement
  while (four<0){
    if (myScanner.hasNextInt()){ // if it is an int
          four= myScanner.nextInt(); // set int to num
            if (four>=0 && four <60 && four!= three && four!=two && four!=one){ // if num is between 1 and 10
              break; //leave loop
            }
         four=-1; //reset num
      }
      else { // if not an int
        String junkWord4 = myScanner.next(); // discard input 
      }
      System.out.println ("Error: enter an int between 0 and 59 with no duplicates"); //prompt to enter a positive integer
    }
  
  System.out.print ("Enter a number between 0 and 59: "); //print statement
  while (five<0){
    if (myScanner.hasNextInt()){ // if it is an int
          five= myScanner.nextInt(); // set int to num
            if (five>=0 && five <60 && five!=four && five!= three && five!=two && five!=one){ // if num is between 1 and 10
              break; //leave loop
            }
         five=-1; //reset num
      }
      else { // if not an int
        String junkWord5 = myScanner.next(); // discard input 
      }
      System.out.println ("Error: enter an int between 0 and 59 with no duplicates"); //prompt to enter a positive integer
    }
 System.out.println ("Your numbers are: " + one + ", " +two+", " +three+", " +four+", " +five);
 int [] user = new int [5];
  user[0] = one;
  user[1] = two;
  user[2] = three;
  user[3] = four;
  user[4] = five;
  
  System.out.print ("The winning numbers are: ");
  int [] winning = new int [5]; 
  winning = numbersPicked();
  System.out.println();
  boolean win = userWins (user, winning);
  if (win == true){
    System.out.println ("You won");
  }
  else {
    System.out.println ("You lose");
  }
  
   } // end of main method
} //end of class