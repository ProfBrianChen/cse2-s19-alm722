//Amanda Morrison
//3-1-19
//CSE 02 section 210

// This program uses loops
// I will take input from the user and do a twist

import java.util.Scanner; // import a scanner

public class TwistGenerator{
  // open class
  public static void main (String[] args) {
    //main method that you need for every java program 
 
    Scanner myScanner = new Scanner(System.in); // making the instance of scanner declared which will take input
    int length=0; // define and declare lenght
    
    while (length<=0 || !myScanner.hasNextInt()) { // while the input is invalid ie not a positive int
       System.out.print ("enter a positive integer "); //prompt to enter a positive integer
      if (myScanner.hasNextInt()){ // if it is an int
        length= myScanner.nextInt(); // set length to int
        if (length>0){ // if length is positive 
          break; // leave loop
        }
      }
      else { // if not an int
        String junkWord = myScanner.next(); // discard input 
      }
    }
    
   int counter=0; // define and initiale counter   
   while (counter < length){ // run length times
      if (counter%3 ==1){ // if counter / 3 has a remainder of 1
        System.out.print(" "); // print a space
      }
      if (counter%3 ==2){ // if counter / 3 has a remainder of 2
        System.out.print("/"); // print a /
      }
      if (counter%3 ==0){ // if counter / 3 has a remainder of 0
        System.out.print("\\"); // print a \ 
      }

      counter++; //inciment counter by one
    }
    counter=0; //redifine counter to 0
    System.out.print("\n"); //new line
    
  while (counter < length){ // run length times
    if (counter%2 ==1){ // if counter / 2 has a remainder of 1
      System.out.print("X"); // print an X
    }
    if (counter%2 ==0){ // if counter / 2 has a remainder of 0
      System.out.print(" "); // print a space
    }
    
  counter++; //incriment counter by one
  }
  counter=0; //redifine counter to 0
  System.out.print("\n"); //new line
    
  while (counter < length){ // run length times
    if (counter%3 ==1){ // if counter / 3 has a remainder of 1
      System.out.print(" "); // print a space
    }
    if (counter%3 ==0){ // if counter / 3 has a remainder of 0
      System.out.print("/"); // print a /
    }
    if (counter%3 ==2){ // if counter / 3 has a remainder of 2
      System.out.print("\\"); // print a \ 
    }
  counter++; //inciment counter by one
  }  
    
  } // end of main method
} //end of class
      