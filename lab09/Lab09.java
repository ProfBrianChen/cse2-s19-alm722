//Amanda Morrison
//4-12-19
//CSE 02 section 210 lab09

// This program uses methods and arrays
// I will compare implement an array search

import java.util.Arrays; //import array
import java.util.Random; //import a random generator
import java.util.Scanner; //import a scanner

public class Lab09{ // open class

public static int [] random (int length){ // random method that takes in the length of the array
  Random randomGenerator = new Random(); //making the instance of random declaration
  int [] random = new int [length]; //generate an array with length
  for (int i = 0; i<length; i++){ //for each index in the array
    random [i] = randomGenerator.nextInt (length); //set the number to a random int less than length
  }
  return random; //take the random array into the main method
}

  public static int [] randinc (int length){ //random incrementing method that takes in the length of the array
  Random randomGenerator = new Random(); //making the instance of random declaration
  int [] random = new int [length]; //generate an array with length
  for (int i = 0; i<length; i++){ //for each index in the array
    random [i] = randomGenerator.nextInt (length); //set the number to a random int less than length
  }
  Arrays.sort(random); //sort the array into ascending order
  return random; //take the random array into the main method
  }
  
  public static int linear(int [] array, int num){ //linear search method
    int where = -1;  // set the index to -1 to indicate not found
    for (int i = 0; i<array.length; i++){ //for each number in array
      if (array[i] == num){ //if the number at that index is equal to number youre looking for
        where = i; //set where to the index
      }
    }
    return where; //returns the index
  }
  
  public static int binary(int [] array, int num){ //binary search method
    int where = -1; //set the index to -1 to indicate not found
    int low = 0; //lowest index youre looking at
    int high = array.length -1; //highest index youre looking at
    while (low<=high){    //while low is less than high      
    int mid = ((low + high) / 2); //set mid equal to the middle of the array
    if (array[mid] == num){     //if the number at index mid is equal to num  
      where = mid; //set where to mid
      break; //leave the while loop
    }
    if (array[mid] > num){ //if the number is below the middle
        high = mid - 1; //reset the high to mid - 1
    }
      if (array[mid] < num){ //if the number is above the middle
        low = mid + 1; //reset the low to mid + 1
      }
    }
    return where; //returns the index
  }

public static void main (String[] args) {  //main method that you need for every java program 
  Scanner myScanner = new Scanner(System.in); // making the instance of the scanner
  int a = 1; //define and initialize a
  System.out.println ("Linear or binary search? "); //prompt user
  String one = myScanner.next(); //set input to one
  while (a !=0){ //while a is not 0
  if (one.equals("linear")){ //if the input is linear
  System.out.print ("Enter an array size "); //print statement
  int length = -1; //define and initialize length
while (length<0){ //while the length is negative
  if (myScanner.hasNextInt()){ // if it is an int
        length= myScanner.nextInt(); // set int to length
          if (length>=0){ // if length is positive
            break; //leave loop
          }
       length=-1; //reset length
    }
    else { // if not an int
      String junkWord = myScanner.next(); // discard input 
    }
    System.out.println ("Error: enter a positive int"); //prompt to enter a positive integer
  }
    System.out.print ("Enter a number to find "); //print statement
  int num = -1; //define and initialize num
while (num<0){ //while num is negative
  if (myScanner.hasNextInt()){ // if it is an int
        num= myScanner.nextInt(); // set int to num
          if (num>=0){ // if num is positive
            break; //leave loop
          }
       num=-1; //reset num
    }
    else { // if not an int
      String junkWord = myScanner.next(); // discard input 
    }
    System.out.println ("Error: enter a positive int"); //prompt to enter a positive integer
  }
    int [] array = new int [length]; //define and initialize array of length 
    array = random(length); //fill array with the return of method random
    
    int where = linear (array, num); // set where to the return of method linear
    for (int b = 0; b<length; b++){ //for each number in the array
      System.out.println (array[b] + " "); //print out the value at that index
    }
    System.out.println ("number is found at " + where); //print out where number is found
    a = 0; //set a to 0 to leave loop
  }
  else if (one.equals("binary")){ //if the input is binary
    System.out.print ("Enter an array size "); //print statement
  int length = -1; //define and initialize length
while (length<0){ //while length is negative
  if (myScanner.hasNextInt()){ // if it is an int
        length= myScanner.nextInt(); // set int to length
          if (length>=0){ // if length is positive
            break; //leave loop
          }
       length=-1; //reset length
    }
    else { // if not an int
      String junkWord = myScanner.next(); // discard input 
    }
    System.out.println ("Error: enter a positive int"); //prompt to enter a positive integer
  }
    System.out.println("Enter a number to find "); //print statement
    int num = -1; //define and initialize num
while (num<0){ //while num is negative
  if (myScanner.hasNextInt()){ // if it is an int
        num= myScanner.nextInt(); // set int to num
          if (num>=0){ // if num is positive
            break; //leave loop
          }      
    num=-1; //reset num
    }
    else { // if not an int      
      String junkWord = myScanner.next(); // discard input 
    }
    System.out.println ("Error: enter a positive int"); //prompt to enter a positive integer
  }
    int [] array = new int [length]; //define and initialize array of length
    array = randinc(length); //fill array with return of method randinc

    int where = binary (array, num); //define and initialize where to the return of method binary
    for (int b = 0; b<length; b++){ //for each index in the array
      System.out.println (array[b] + " "); //print out the number
    }
    System.out.println ("number is found at " + where); //print out the index 
    a = 0; //set a to 0 to leave loop
  }
  else { //if the input is neither
    System.out.println ("try again. enter either 'linear' or 'binary' "); //reprompt user
    one = myScanner.next(); //reset one
  }
  }
  
  
 } // end of main method
} //end of class