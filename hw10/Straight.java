//Amanda Morrison
//4-19-19
//CSE 02 section 210 hw10

// This program will evaluate a random generation of a poker hand and determine if you have a straight
// I will use arrays and sorts


import java.util.*; //import all

public class Straight{ //open class
  
  public static int [] shuffle (){ //method shuffle returns an array
    int p = 1; // first card
    int[] deck = new int [52]; //declare and allocate array for the deck with 52 members
    for (int i = 0; i< deck.length; i+=4){ //for every four indexes in the array
      deck[i] = p; //set deck of i to p
      deck[i+1] = p; //set deck of i+1 to p
      deck[i+2] = p; //set deck of i+2 to p
      deck[i+3] = p; //set deck of i+3 to p
      p++; //increase p
   }
   int index, temp; //declare temp and index;
   Random myRandom = new Random(); //making the instance of a random declaration
    for (int j = 0; j<deck.length; j++){ //for each card in the deck
      index = myRandom.nextInt (j+1); //set index to a random number in the deck
      temp = deck[index]; // store the value of deck at that index to temp
      deck[index] = deck [j]; //deck at index = deck at j 
      deck [j] = temp; //set deck at j to the temp variable
    }
    return deck; //return the shuffled deck to the main method
  }
  
  public static int [] hand (int [] array){ //method hand takes in and returns an array
    int[] hand = new int [5]; //declare and allocate space for array
    for ( int i = 0; i < hand.length; i++ ){ //for each index of the hand
      hand [i] = array[i]; //fill the new array with the old array from the top
    }
    return hand; //return the hand to the main method
  }
  
  public static int lowestCard (int [] array, int k) { //method lowestCard returns an int and takes in an array and an int
    if (k>5 || k<=0){ //if k is not between 1 and 5
      System.out.println ("Error"); //error statement
      return -1; //return an invalid number
    }
    int found = 0; //declare and initialize
    int low = 9999; //declare and initialize
    int index = -1; //declare and initialize
    int [] test = new int [5]; //create new array of length 5
    for (int i = 0; i<array.length; i++){ //for each number in array
      test [i] = array [i]; //fill test with values in array
    }
    for(int j = 0; j < k; j++){ //for k number of times
       for (int i = 0; i<test.length; i++){ //for each index in the array
        if (test[i] < low && test[i] != -1){ //if number at that index is lowerer than low and not 1
           low = test[i]; //set low to that number       
           index = i; //set index to i
        }
      }
      found = low; //found is equal to the low
      test[index] = -1; //set the value to -1 so it wont be counted again in search
      low = 9999; //reset low
    }
    return found; //return found from method
  }
  
  public static boolean straight (int [] array){ //method straight returns a boolean and takes in an array
    boolean straight = false; //declare and initialize straight
    int c = 0; //declare and initialize
    for (int i = 0; i<array.length-1; i++){ //for one less than the array length (number of comparisons )
      if (lowestCard(array, i+1) == lowestCard(array, i+2)-1){ //if the i+1 lowest card is equal to one less than the i+2 lowest card
        c++; //increase c
      }
    }
    if (c == 4){ //if c is four (all comparisons entered the if statement)
      straight = true; //set straight to true
    }
    return straight; //return true
  } 
  
   public static void main (String[] args){ //main method
     double count = 0; //declare and initialize
     for (int i = 0; i<1000000; i++){ //run one million times
       int [] y = shuffle(); //run shuffle method and store in array y
       int [] x = hand(y); //run hand method and store in array x
       if (straight(x)) { //if the boolean condition is true (ie there is a straight)
         count++; //increase counter
       }
     }
     double percent = count/10000.00; //finding the percent of the times you get a straight
     System.out.println (percent + "%"); //print out the percentage 
     
   } //close main method
} // close class