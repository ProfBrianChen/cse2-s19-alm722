//Amanda Morrison
//4-19-19
//CSE 02 section 210 hw10

// This program uses 2D arrays
// I will print and operate on matrices


import java.util.*; //import all

public class RobotCity{ //open class
  
  public static int [] [] buildCity(){ //method buildCity returns a 2D array
    Random randomGenerator = new Random(); //making the instance of random declaration
    int [] [] size = new int [randomGenerator.nextInt (6) + 10] [randomGenerator.nextInt (6) + 10]; //declare and allocate space for the 2D array
    for (int i = 0; i<size.length; i++){ //for each number in the first array
      for (int j = 0; j<size[0].length; j++){ //for each number in the second array
         size[i][j] = randomGenerator.nextInt(900)+100; //at that position generate a number between 100 and 999
      }
    }
    return size; //send the 2D array to the main method
  }
  
  public static void display (int [] [] array){ //method display returns nothing and takes in a 2D array
    for(int i = 0; i < array[0].length; i++){ //for each number in the second array
       for(int j = 0; j < array.length; j++) { //for each number in the first array
          System.out.printf("%4d ", array[j][i]); //print out the value at those indexes with the appopriate spacing
       }
    System.out.println(); //print a new line
    }
  }
  
  public static void invade (int [] [] array, int k){ //method invade returns nothing and takes in a 2D array and an int
    Random randomGenerator = new Random(); //making the instance of random declaration
    for (int i = 0; i<k; i++){ //for k times
      int one = randomGenerator.nextInt(array.length); //one is a random number between 0 and the length of the first array
      int two = randomGenerator.nextInt(array[0].length); //two is a random number between 0 and the length of the second array
      if (array[one][two] >0){ //if the array is positivie there
        array[one][two] = - (array[one][two]); //make the number there negative
      }
      else{ //if the array is already negative there
        i--; //run the loop again (try again until you find a positive)
      }
    }
  }
  
  public static void update (int [][]size){ //method update returns nothing and takes in a 2D array
    for (int i = size[0].length-1; i>=0; i--){ //for length of second array number of times
      for (int j = size.length-1; j>=0; j--){ //for length of first array number of times
         if (size[j][i] < 0){ //if the number there is negative
           if (j == size.length-1) { // if j is at the end of the array
             size[j][i] = - (size[j][i]); //make the number there positive 
           } else { //if j is not at the end of the array
             size[j][i] = -(size[j][i]); //make the number there positive
             size[j+1][i] = -(size[j+1][i]); //make the number to the right of it negative
           }
         }
      }
    }  
  }
  
  public static void main (String [] args){ //main method
    Random randomGenerator = new Random(); //making the instance of a random declaration
    int [] [] size = buildCity(); //make an array from the method buildCity
    display(size); //call the display method with array input
    int k = (randomGenerator.nextInt(10) + 1); // k is a random number between 1 and 10
    invade(size, k); //call the invade method with the array and k as input
    System.out.println(); //new line
    display(size); //call the display method with array input
    for (int i = 0; i<5; i++){ //do this loop 5 times
      System.out.println(); //new line
      update (size); //call the update method with array input
      display(size); //call the display method with array input
    }
    
    
  } //end of main method
} //end of class