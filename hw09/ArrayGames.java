//Amanda Morrison
//4-12-19
//CSE 02 section 210 hw09

// This program uses methods and arrays
// I will write three methods - print, insert, and shorten

import java.util.Arrays; //import array
import java.util.Random; //import a random generator
import java.util.Scanner; //import a scanner

public class ArrayGames{ // open class
  
  public static int [] generate (){ //method generate that returns an array
    Random randomGenerator = new Random(); //making the instance of random declaration
    int length = randomGenerator.nextInt (11) + 10; //make length a random number between 10 and 20
    int [] array = new int [length]; //generate an array with length
    for (int i = 0; i<array.length; i++){ //for each index in the array
      array[i] = randomGenerator.nextInt (21); //generate a num for that index less than 21
    }
    return array;  // return the array to the main method
  }
  
  public static void print (int [] array){ // print method that returns nothing
    System.out.print ("Input 1: {"); //print statement
        for (int a = 0; a<(array.length - 1); a++){ //for each index in the array excpet the last one
          System.out.print (array[a] + ", "); //print out the num in the array
        }
        System.out.println (array[array.length-1] + "}"); //print out the last num in array 
  }
  
  public static int [] insert(int [] array, int [] array2){ //insert method that takes in two arrays
    Random randomGenerator = new Random(); //making the instance of random declaration
    int [] combine = new int [array.length + array2.length]; // declare and allocate combine array
    int num = randomGenerator.nextInt(array.length); // generate a random number
    int p = 0; // define and initialize index counter p for new array combine
    System.out.print ("Output: {"); //print statement
    for (int a = 0; a<num; a++){ //for each index in array until num is reached
      System.out.print (array[a] + ", "); //print out the number
      combine [p] = array[a]; //set the number to correct index in combine
      p++; //inciment p
    }
    for (int b = 0; b<array2.length; b++){ //for each index in array2
      System.out.print (array2[b] + ", "); //print out the number
      combine [p] = array2[b]; //set the number to the correct index in combine
      p++; //inciment p
    }
    for (int c = num; c<(array.length -1); c++){ //for the rest of the indexes in array except for the last one
      System.out.print (array[c] + ", "); //print out the number
      combine [p] = array[c]; //set the number to the correct index in combine
      p++; //inciment p
    }
    System.out.println (array[array.length-1] + "}"); //print out the last num in array
    return combine; //return the combine array
  }

  public static int [] shorten(int [] array, int index){ //shorten method that takes in an array and an int
    int [] shorter = new int [array.length - 1]; //declare and allocate shorter array
    int p = 0; //define and initialze index counter for shorter  
    
    if (index >= array.length){  // if the user input exceeds the array length
      return array; // return the original array to main method
     }
    else { //if the input is within the array index
        for (int a = 0; a<(index); a++){ //for each number in the array until the index is reached
            shorter [p] = array[a]; //set array at that index to shorter at p
            p++; //inciment p
        }
        for (int b = (index+1); b<array.length; b++){ //for each number in the array after the index
              shorter [p] = array[b]; //set array at that index to shorter at p
              p++; //incriment p
        }  
       return shorter; //return the shorter array to the main method
    }
  }
  
  public static void main (String[] args) {  //main method that you need for every java program 
  Scanner myScanner = new Scanner(System.in); // making the instance of the scanner
  
    System.out.println ("Do you want to run, insert, or shorten the string? "); //prompt user
    String ans = myScanner.next(); //set the user input to ans
    
    int i = 0; //define and initialize 
    while (i == 0){ //while i is 0
      if (ans.equals("run")){ //if the input is run
        int [] array = generate(); //make array from the method generate
        print (array); //run print method 
        i++; //inciment i
      }

      else if (ans.equals ("insert")){ //if the input is insert
        int [] array = generate(); //make an array from the method generate
        int [] array2 = generate(); //make an array from the method generate
        System.out.print ("Input 1: {"); //print statement
        for (int a = 0; a<(array.length - 1); a++){ //for each index in the array except for the last one
          System.out.print (array[a] + ", "); //print out the num
        }
        System.out.println (array[array.length-1] + "}"); //print out the last num in the array
        
        System.out.print ("Input 2: {"); // print statement
        for (int b = 0; b<(array2.length - 1); b++){ //for each index in the array2 except for the last one
          System.out.print (array2[b] + ", "); //print out the num
        }
        System.out.println (array2[array2.length-1] + "}"); //print out the last num in the array
        int [] combine = insert (array, array2);  //make combine array from insert method
        i++; //inciment i 
      }

      else if (ans.equals ("shorten")){ //if the input is shorten
        System.out.println ("What index do you want to remove? "); //prompt user
          int index = -1; //define and initialize index
          while (index<0){ //while the index is negative
            if (myScanner.hasNextInt()){ // if it is an int
                  index= myScanner.nextInt(); // set int to index
                    if (index>=0){ // if index is positive
                      break; //leave loop
                    }
                 index=-1; //reset index
              }
              else { // if not an int
                String junkWord = myScanner.next(); // discard input 
              }
              System.out.println ("Error: enter a positive int"); //prompt to enter a positive integer
            }
          int [] array = generate(); //create an array using the generate method
        
        System.out.print ("Input 1: {"); //print statement
              for (int c = 0; c<(array.length - 1); c++){ //for each index in array except for the last one
                System.out.print (array[c] + ", "); //print out the num
              }
            System.out.println (array[array.length-1] + "}"); //print out the last num in array
        
          System.out.println ("Input 2: " + index); //print out second input
           System.out.print ("Output: {"); //print statement
         int [] end = shorten (array, index);
              for (int b = 0; b<(end.length - 1); b++){ //for each index in array except for the last one
                System.out.print (end[b] + ", "); //print out the num
              }
            System.out.println (end[end.length-1] + "}"); //print out the last num in array
        i++; //inciment i
      }

      else { //if the input in none of them
        System.out.println ("try again. enter either 'run', 'insert', 'shorten' "); //reprompt user
        ans = myScanner.next(); //reset ans
      }
    }
     } // end of main method
} //end of class