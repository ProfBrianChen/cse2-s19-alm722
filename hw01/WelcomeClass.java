///////////
///CSE 02 welcome class
/// Amanda Morrison 
///

public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints statements to terminal window
    System.out.println("    -----------");
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\"); //use \\ to avoid escape sequences, it only displays as \
    System.out.println("<-A--L--M--7--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    System.out.println("My name is Amanda and I'm from near Princeton, NJ.\nI love skiing and going to the beach");
  }
  
}