//Amanda Morrison
//3-22-19
//CSE 02 section 210 hw07

// This program uses methods
// I will calculate the area 

import java.util.Scanner; // import a scanner

public class Area{
  // open class
  
public static double Valid (String input1, double input2, String shape){   //method Valid that returns a double and takes 2 strings and one double as input
    Scanner myScanner = new Scanner(System.in); // making the instance of scanner declared which will take input
    System.out.print ("Enter the " + input1 + " of the " + shape); // prompt user
    while (!myScanner.hasNextDouble() || input2<=0) { // while the input is invalid ie not an int or 0 or negative
       if (myScanner.hasNextDouble()){ // if it is an int
        input2= myScanner.nextDouble(); // set int to input
            if (input2>0){ // if input is between 1 and 10
              break; //leave loop
            }
         input2=0; //reset input
      }
      else { // if not an int
        String junkWord = myScanner.next(); // discard input 
      }
      System.out.println ("Error: enter a " + input1); //prompt to enter a positive integer
    }
  return input2; //bring input2 down to main method
}
  
  public static double Rectangle (double length, double width){ //method to calculate rectangle area
    double area = length * width; // area is length x width
    return area; //bring area down to main method
  }
    public static double Triangle (double length, double height){ //method to calculate triangle area
    double area = length * height / 2; //area is half of length x height
    return area; //bring area down to main method
  }
    public static double Circle (double radius){ //method to calculate circle area
    double area =3.1415 * (radius * radius); //area is pi r^2
    return area; //bring area down to main method
  }
  
public static void main (String[] args) { 
    //main method that you need for every java program 
Scanner myScanner = new Scanner(System.in); // making the instance of scanner declared which will take input

int i = 0; //define and initialze i
String next = ""; //define and initialze next
System.out.println ("What shape do you want to calculate the volume of (rectangle, triangle, or circle)"); //prompt user
   while ( i != 1){ //while i is not 1
     next = myScanner.next(); //set next to the input
     if(next.equals("rectangle") || next.equals("triangle") || next.equals("circle")){ //if a correct shape is entered
       i = 1; //set i to 1 to leave loop
     }
     else{ // if correct shape was not entered
      System.out.println ("Invalid shape entered. Enter either 'rectangle', 'triangle', or 'circle' "); //prompt user
     }
    }
  if (next.equals("rectangle")){ //if rectangle was typed in
    double length = Valid ("length", 0, "rectangle"); //get a valid input 
    double width = Valid ("width", 0, "rectangle"); //get a valid input 
    double area = Rectangle(length, width); //run method to calculate area
    System.out.println("The area of your rectangle is: "+area); //print statement
 
  }
  else if (next.equals("triangle")){ //if triangle was typed in
    double height = Valid ("height", 0, "triangle"); //get a valid input 
    double length = Valid ("length", 0, "triangle"); //get a valid input 
    double area = Triangle(height, length); //run method to calculate area
    System.out.println("The area of your triangle is: "+area); //print statement
  }
  else if (next.equals("circle")){ //if circle was typed in
    double radius = Valid ("radius", 0, "circle"); //get a valid input 
    double area = Circle(radius); //run method to calculate area
    System.out.println("The area of your circle is: "+area); //print statement
  }
  
   } // end of main method
} //end of class