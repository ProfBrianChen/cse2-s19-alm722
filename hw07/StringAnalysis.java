//Amanda Morrison
//3-22-19
//CSE 02 section 210 hw07

// This program uses methods
// I will determine if a string contains letters and process a specific number of characters in the string

import java.util.Scanner; // import a scanner

public class StringAnalysis{
  // open class
  
  public static boolean AllLetter (String input1, int input2){ //runs if an int is given
    int p = 0; //initialize p
    boolean letters = true; //initialize letters
    if (input2<input1.length()){ //if the length entered is less than the entire length
    for (int i = 0; i< input2; i++){ // for the length entered
      char letter= input1.charAt(i); //for char at position i
      if (letter>'z' || letter<'a'){ //if the char is not a lowercase letter
        p++; //incriment p by 1
      }
    }
    } else { //if the length entered is longer than the total length 
      for (int i = 0; i< input1.length(); i++){ // for the length of the string
      char letter= input1.charAt(i); // for char is position i
      if (letter>'z' || letter<'a'){ //if the char is not a lowercase letter
        p++; //incriment p by 1
      }
    }
      
    }
    if (p != 0){ //if p is not 0
      letters = false; //letters is false
    }
    
    return letters; //bring letters down to the main method
  }
 
    public static boolean AllLetter (String input1){ //runs if no int is given
    int p = 0; // initialize p
    boolean letters = true; //initialize letters
    for (int i = 0; i< input1.length(); i++){ //for the length of the string entered
      char letter= input1.charAt(i); //for char in posiiton i
      if (letter>'z' || letter<'a'){ //if the char is not a lowercase letter
        p++; //increase p by 1
      }
    }
    if (p!=0){ //if p is not 0
      letters = false; //set letters to false
    }
    return letters; //bring letters down to the main method
  }
  
  
public static void main (String[] args) {
    //main method that you need for every java program 
 Scanner myScanner = new Scanner(System.in); // making the instance of scanner declared which will take input
 int num=0; //define and initialize num
  int j = 0; //define and initialize j
  boolean end = true; //define and initial end
  
  System.out.print ("Enter your string: "); //prompt user
  String string = myScanner.nextLine(); //capture the string
  System.out.print ("Do you want to analyze the whole string? "); //prompt user
  String whole = myScanner.nextLine(); //capture the user answer
  while (j == 0){ //run until j is no longer 0
  if (whole.equals("no")){ //if they entered no
    System.out.print ("How many characters do you want to analyze? "); //prompt user
     while (!myScanner.hasNextInt() || num<=0) { // while the input is invalid ie not an int or 0 or negative
         if (myScanner.hasNextInt()){ // if it is an int
          num= myScanner.nextInt(); // set int to num
            if (num>0){ // if num is between 1 and 10
              break; //leave loop
            }
         num=0; //reset num
      }
      else { // if not an int
        String junkWord2 = myScanner.next(); // discard input 
      }
      System.out.println ("Error: enter a positive int"); //prompt to enter a positive integer
    }
    end = AllLetter (string, num); //set end to the return of program all letter
    j++; //increase j
}
  else if (whole.equals("yes")){ //if the user entered yes
    end = AllLetter (string); //after running the all letter method
    j++; //increase j
  }
  else { //if user didnt answer yes or no
    int i = 0; //define and initialize i
     while ( i != 1){ // runs while i isnt one
     if(whole.equals("yes") || whole.equals("no")){ //if the user enters yes or no
       i = 1; //set i to 1
     }
     else{  //if the user didnt enter yes or no
      System.out.println ("Error: enter either yes or no "); //print error statement
      whole = myScanner.next(); //set whole to new input
     }
   }
 }
  }
  
  if (end == true){ //if end is true
    System.out.println ("All characters are letters");  //all the characters are letters
  }
  else { //if end is false
    System.out.println ("All characters are not letters"); //not all the characters are letters 
  }

    } // end of main method
} //end of class