//Amanda Morrison
//2-1-19
//CSE 02 section 210

// cyclometer measure elapsed time in seconds and number of roatations of the front wheel of a bike
// the programs prints number of minutes, number of counts, distance in miles, and the distance of two trips

public class Cyclometer{
  //main method
  public static void main (String[] args){
    //input data
    int secsTrip1=480; //time in seconds of trip one
    int secsTrip2=3220; // time in seconds of trip two
    int countsTrip1=1561; //number of counts in trip one
    int countsTrip2=9037; // number of counts in trip two
    
    //intermediate variables and output data
    double wheelDiameter=27.0, //the diameter of the wheel stored as about
    PI=3.14159, //approx value for pi
    feetPerMile=5280, // the number of feet in one mile
    inchesPerFoot=12, // the number of inches in one foot
    secondsPerMinute=60; //the number of seconds per one minute
    double distanceTrip1, distanceTrip2, totalDistance; // creation of variables
    
    //print statements and converting units
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+ " counts."); // print statement summaraizing data for trip one and converting seconds to minutes
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+ " counts."); // print statement summaraizing data for trip two and converting seconds to minutes
    
    //run calculations and store values
    distanceTrip1=countsTrip1*wheelDiameter*PI; //calculate distance in inches. one rotation occurs per count. diameter in inches multiplied by pi
    distanceTrip1/=inchesPerFoot*feetPerMile; //distance in miles for trip one
    distanceTrip2=countsTrip2*wheelDiameter*PI; //calculate distance in inches. one rotation occurs per count. diameter in inches multiplied by pi
    distanceTrip2/=inchesPerFoot*feetPerMile; //distance in miles for trip two
    totalDistance=distanceTrip2+distanceTrip1; //adding both distances together to find totalDistance
    
    //print out distances
    System.out.println("Trip 1 was "+distanceTrip1+ " miles"); //print statement for trip one distance in miles
    System.out.println("Trip 2 was "+distanceTrip2+ " miles"); //print statement for trip two distance in miles
    System.out.println("The total distance was "+totalDistance+ " miles"); //print statement for both trip distances in miles
    
  } // end of main method 
} //end of class