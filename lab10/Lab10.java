//Amanda Morrison
//4-19-19
//CSE 02 section 210 lab10

// This program uses 2D arrays
// I will print and operate on matrices

import java.util.Arrays; //import array
import java.util.Random; //import a random generator
import java.util.Scanner; //import a scanner

public class Lab10{ // open class
  
  public static int [][] increasingMatrix (int width, int height, boolean format){
    int p = 1;
    int [][] array;
    if (format){
      array = new int [height][width];
      for (int j = 0; j<height; j++){
        for (int i = 0; i< width; i++){
          array[j][i] = p;
          p++;
        }
      }
      
    } else {
      array = new int [width][height];
      for (int j = 0; j<height; j++){
        for (int i = 0; i< width; i++){
          array[i][j] = p;
          p++;
        }
      }
      
    }
    return array;
  }
  
  public static void printMatrix (int [][]x, boolean format){  
      if (x == null){
        System.out.println ("the array was empty!");
      }
    else if (format){
     for(int i = 0; i < x[0].length; i++){ //for each number in the second array
       for(int j = 0; j < x.length; j++) { //for each number in the first array
          System.out.printf("%4d ", x[j][i]); //print out the value at those indexes with the appopriate spacing
       }
    System.out.println(); //print a new line
    }
      }
    else {
     for(int i = 0; i < x[0].length; i++){ //for each number in the second array
       for(int j = 0; j < x.length; j++) { //for each number in the first array
          System.out.printf("%4d ", x[j][i]); //print out the value at those indexes with the appopriate spacing
       }
    System.out.println(); //print a new line
    }
    }
  }
  
  public static int [][] translate(int[][]array){
    int [][]newArray = new int [array[0].length][array.length];
    for (int j = 0; j<array.length; j++){
        for (int i = 0; i< array[0].length; i++){
          newArray[j][i] = array[i][j];
        }
      }
    return newArray;
  }
  
  public static int [][] addMatrix(int [][] a, boolean formatA, int[][] b, boolean formatB){
     if (formatA == formatB) {
      if (a.length == b.length && a[0].length == b[0].length) {
        int[][] C = new int[a.length][a[0].length];
        for (int i = 0; i < C.length; i++) {
          for (int j = 0; j < C[0].length; j++) {
            C[i][j] = a[i][j] + b[i][j];
          }
        }
        if (formatA == false && formatB == false) {
          C = translate(C);
        }
        return C;
      } else {
        System.out.println("the arrays can't be added");
        return null;
      }
    } else {
      if (a.length == b[0].length && a[0].length == b.length) {
        if (formatA == false) {
          a = translate(a);
        } else if (formatB == false) {
          b = translate(b);
        }
        int[][] C = new int[a.length][a[0].length];
        for (int i = 0; i < C.length; i++) {
          for (int j = 0; j < C[0].length; j++) {
            C[i][j] = a[i][j] + b[i][j];
          }
        }
        return C;
      } else {
        System.out.println("the arrays can't be added");
        return null;
      }
    }
  }
  
    public static void main (String[] args) {  //main method that you need for every java program 
     Random randomGenerator = new Random();
    int Width1 = randomGenerator.nextInt((5-1) + 1) + 1;
    int Height1 = randomGenerator.nextInt((5-1) + 1) + 1;
    int Width2 = randomGenerator.nextInt((5-1) + 1) + 1;
    int Height2 = randomGenerator.nextInt((5-1) + 1) + 1;
    int[][] a = increasingMatrix(Width1, Height1, true);
    System.out.println("Matrix A:");
    printMatrix(a, true);
    System.out.println();
    int[][] b = increasingMatrix(Width1, Height1, false);
    System.out.println("Matrix B:");
    printMatrix(b, false);
    System.out.println();
    int[][] C = increasingMatrix(Width2, Height2, true);
    System.out.println("Matrix C:");
    printMatrix(C, true); 
    System.out.println();
    int[][] AB = addMatrix(a, true, b, false);
    System.out.println("Matrix A + B:");
    printMatrix(AB, true);
    System.out.println();
    System.out.println("Matrix A + C:");
    int[][] AC = addMatrix(a, true, C, false);
    if (AC != null) {
      printMatrix(AC, true);
    }    
     } // end of main method
} //end of class