//Amanda Morrison
//2-8-19
//CSE 02 section 210

// This program splits the check evenly between x number of people with a tip
// I will take input from the user and perform calculations with inputs

import java.util.Scanner; // import a scanner

public class Check{
  // open class
  public static void main (String[] args) {
    //main method that you need for every java program 
    
    Scanner myScanner = new Scanner(System.in); // making the instance of scanner declared which will take input
    System.out.print ("Enter the original cost of the check in the form xx.xx: "); //prompt to enter cost of check and will go to the beginning of the next line after "Enter:.."
    double checkCost= myScanner.nextDouble(); // store the user input into the variable and send to the next line
    System.out.print( "Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); // prompt to enter the tip and will go to the beginning of the next line
    double tipPercent = myScanner.nextDouble (); // store the user input into the variable and send to the next line
    tipPercent /= 100; // convert the percent into a decimal value
    System.out.print ("Enter the number of people who went out to dinner: "); //prompt to enter number of people at dinner
    int numPeople = myScanner.nextInt (); // store the user input into the variable and send to the next line
    double totalCost; //initate variable 
    double costPerPerson; //initiate variable 
    int dollars, //whole dollar amount of cost
    dimes, pennies; //for storing digits to the right of decimal point for the cost
    totalCost= checkCost * (1+tipPercent); // calculate the total cost including the tip 
    costPerPerson= totalCost/numPeople; //divide the total cost by number of people to find cost per person
    dollars=(int) costPerPerson; //get the whole amount, dropping decimal fraction 
    dimes=(int)(costPerPerson * 10) %10; //get dimes amount where the % is mod and returns the remainder after division 
    pennies=(int)(costPerPerson*100) %10; // get pennes amount where the % is mod and returns the remainder after division 
    System.out.println("Each person in the group owes $"+ dollars + '.' + dimes+ pennies); //print statement in form of dollars and cents
    
    
  } // end of main method
} //end of class