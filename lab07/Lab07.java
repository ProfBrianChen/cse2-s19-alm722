//Amanda Morrison
//3-22-19
//CSE 02 section 210 lab07

// This program uses methods
// I will generate a random story

import java.util.Scanner; // import a scanner
import java.util.Random; //import a random generator 

public class Lab07{
  // open class
  
  public static String adjective (int input){
    //method for adjectives
    String adjString;
    switch (input){ //switch statment for adjString
      case 0:
        adjString = "lazy";
        break;
      case 1:
        adjString = "scary";
        break;
      case 2: 
        adjString = "fluffy";
        break;
       case 3:
        adjString = "blue";
        break;
      case 4: 
        adjString = "soft";
        break;
      case 5:
        adjString = "shiny";
        break;
      case 6: 
        adjString = "happy";
        break;
      case 7:
        adjString = "quick";
        break;
      case 8: 
        adjString = "sad";
        break;
      case 9:
        adjString = "red";
        break;
      default:
        adjString = "Invalid input";
        break;
    }
    return adjString; //send adj String to main method
  }
  
    public static String nounSubj (int input){ //method for adjectives
      String adjString;
    switch (input){ //switch statment for adjString
      case 0:
        adjString = "shark";
        break;
      case 1:
        adjString = "fox";
        break;
      case 2: 
        adjString = "dog";
        break;
       case 3:
        adjString = "cat";
        break;
      case 4: 
        adjString = "mouse";
        break;
      case 5:
        adjString = "pig";
        break;
      case 6: 
        adjString = "cow";
        break;
      case 7:
        adjString = "sheep";
        break;
      case 8: 
        adjString = "fish";
        break;
      case 9:
        adjString = "boy";
        break;
      default:
        adjString = "Invalid input";
        break;
    }
    return adjString; //send adj String to main method
  }
  
    public static String verbs (int input){ //method for adjectives
      String adjString;
    switch (input){ //switch statment for adjString
      case 0:
        adjString = "made";
        break;
      case 1:
        adjString = "jumped";
        break;
      case 2: 
        adjString = "ran to ";
        break;
       case 3:
        adjString = "passed";
        break;
      case 4: 
        adjString = "danced to";
        break;
      case 5:
        adjString = "sang to";
        break;
      case 6: 
        adjString = "skipped to";
        break;
      case 7:
        adjString = "flew to";
        break;
      case 8: 
        adjString = "left";
        break;
      case 9:
        adjString = "drove to";
        break;
      default:
        adjString = "Invalid input";
        break;
    }
    return adjString; //send adj String to main method
  }
  
    public static String nounObj (int input){ //method for adjectives
      String adjString;
    switch (input){ //switch statment for adjString
      case 0:
        adjString = "store";
        break;
      case 1:
        adjString = "mountain";
        break;
      case 2: 
        adjString = "fence";
        break;
       case 3:
        adjString = "house";
        break;
      case 4: 
        adjString = "beach";
        break;
      case 5:
        adjString = "desert";
        break;
      case 6: 
        adjString = "farm";
        break;
      case 7:
        adjString = "field";
        break;
      case 8: 
        adjString = "school";
        break;
      case 9:
        adjString = "valley";
        break;
      default:
        adjString = "Invalid input";
        break;
    }
    return adjString; //send adj String to main method
  }
  
  public static String thesis (){ //main method for thesis 
    Random randomGenerator = new Random(); //making the instance of random declaration
    
    String adj1 = adjective(randomGenerator.nextInt(10)); //run adjective with a random number
    String adj2 = adjective(randomGenerator.nextInt(10)); //run adjective with a random number
    String noun1 = nounSubj(randomGenerator.nextInt(10)); //run noun with a random number
    String verb1 = verbs(randomGenerator.nextInt(10)); //run verb with a random number
    String adj3 = adjective(randomGenerator.nextInt(10)); //run adjective with a random number
    String noun2 = nounObj(randomGenerator.nextInt(10)); //run noun with a random number
    
    System.out.println ("The "+ adj1 + ", " + adj2 + " " + noun1 + " " + verb1 + " the " + adj3 + " " + noun2);
   
    return noun1; //send noun1 to main method
  }
  
  public static String action (int input, String noun1){ //action method
    
    for (int i = 0; i<input; i++){ //for a random number of times
    Random randomGenerator = new Random(); //making the instance of random declaration
    int rand1 = randomGenerator.nextInt(10); //generate random numbers
    int rand2 = randomGenerator.nextInt(10);
    int rand3 = randomGenerator.nextInt(10);
    int rand4 = randomGenerator.nextInt(10);
    int rand5 = randomGenerator.nextInt(10);
    
    String adj1 = adjective(rand1); ///run adjective with a random number
    String adj2 = adjective(rand2); //run adjective with a random number
    String verb1 = verbs(rand3); //run verb with a random number
    String adj3 = adjective(rand4); //run adjective with a random number
    String noun2 = nounObj(rand5); //run noun with a random number
    
    System.out.println ("The "+ noun1 + " was particularly " + adj1 + " to " + verb1 + " " + noun2);
    }
    return noun1; //send noun1 to main method
  }
  
  public static String conclusion (String noun1){ //conclusion method
    Random randomGenerator = new Random(); //making the instance of random declaration 
    String verb1 = verbs(randomGenerator.nextInt(10)); //run verb with a random number
    String noun2 = nounObj(randomGenerator.nextInt(10)); //run noun with a random number
    int random = randomGenerator.nextInt(1); // generate a random number
    if (random == 1){ // if random is one
      System.out.print ("That " + noun1); //print this then noun
    }
    else { // if random is 0
      System.out.print ("It "); //print it
    }
    System.out.println (" " + verb1 + " her " + noun2 + "!"); //generate sentence
    return noun1; //send noun1 to main method
  }
  
  public static void main (String[] args) {
    //main method that you need for every java program 
 Scanner myScanner = new Scanner(System.in); // making the instance of scanner declared which will take input
 Random randomGenerator = new Random(); //making the instance of random generator 
    
    String subject = thesis(); //run thesis and return noun
   
    System.out.println ("Another sentence? enter 'yes' to continue "); //prompt user
    String next = myScanner.next(); //take input
  
    if (next.equals("yes")){ //if user types yes
      String act = action(randomGenerator.nextInt(5), subject); //call action method
    }
    
    String act = action(randomGenerator.nextInt(5), subject); //call action method
      String conc = conclusion (subject); //call conclusion method 
    
        } // end of main method
} //end of class