//Amanda Morrison
//3-8-19
//CSE 02 section 210 lab06

// This program uses loops
// I will ask users for an integer between 1-10 and ensure correct type and range

import java.util.Scanner; // import a scanner

public class PatternC{
  // open class
  public static void main (String[] args) {
    //main method that you need for every java program 
 
    int num = 0; // define and initalize number
    
    Scanner myScanner = new Scanner(System.in); // making the instance of scanner declared which will take input
    
    System.out.print ("Enter integer between 1 and 10 "); //prompt to enter an integer between 1 and 10
    while (!myScanner.hasNextInt() || num<=0) { // while the input is invalid ie not an int or 0 or negative
       if (myScanner.hasNextInt()){ // if it is an int
        num= myScanner.nextInt(); // set int to num
            if (num>0 && num<11){ // if num is between 1 and 10
              break; //leave loop
            }
         num=0; //reset num
       }
      else { // if not an int
        String junkWord = myScanner.next(); // discard input 
      }
      System.out.print ("Error: enter an int betweeen 1 and 10 "); //prompt to enter a positive integer
    }
    
   for(int numRows = 1; numRows <= num; numRows++){ // make num number of rows
      for(int i = num; i > numRows; i--) { // run this loop one less time than the last starting with num 
        System.out.print(" "); // print a space to make the numbers justify right 
      }
        for (int value=numRows; value>0; value--){ // value increases to num so it prints out the increasing values to the left
          System.out.print (value); //print the value
        }
      System.out.println(); // new line
    }
    
      } // end of main method
} //end of class