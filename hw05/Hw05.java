//Amanda Morrison
//3-1-19
//CSE 02 section 210

// This program uses loops
// I will ask users about their class and ensure correct type

import java.util.Scanner; // import a scanner

public class Hw05{
  // open class
  public static void main (String[] args) {
    //main method that you need for every java program 
 
    int course = 0; // define and initalize course number
    String department = ""; //define and initalize department name
    int number = 0; // define and initalize number of times per week the class meets
    int time = 0; // define and initalize time the class starts
    String instructor = ""; //define and initalize instructor name
    int student = 0; //define and initalize the number of students
    
    Scanner myScanner = new Scanner(System.in); // making the instance of scanner declared which will take input
    
    System.out.print ("Enter course number "); //prompt to enter a positive integer
    while (!myScanner.hasNextInt() || course<=0) { // while the input is invalid ie not an int or 0 or negative
       if (myScanner.hasNextInt()){ // if it is an int
        course= myScanner.nextInt(); // set course to int
            if (course>0){ // if course is positive
              break; //leave loop
            }
      }
      else { // if not an int
        String junkWord = myScanner.next(); // discard input 
      }
      System.out.print ("Error: enter course number as a positive int "); //prompt to enter a positive integer
    }
    
    System.out.print ("Enter a department "); //prompt to enter a department
    while (myScanner.hasNextInt() || myScanner.hasNextDouble()){ //while the input is invalid ie a number
      String junkWord = myScanner.next(); // discard input
      System.out.print ("Error: enter course number as an explicit string (no numbers) "); //prompt user to enter something that isnt a number
    }
    department= myScanner.next(); // set department to string
    
    System.out.print ("Enter number of times the course meets per week "); //prompt to enter a positive integer
    while (!myScanner.hasNextInt() || number<=0) { // while the input is invalid ie not a positive int
        if (myScanner.hasNextInt()){ // if it is an int
        number= myScanner.nextInt(); // set number to int 
          if (number>0){ //if number is positive
            break;// leave loop
          }
      }
      else { // if not an int
        String junkWord = myScanner.next(); // discard input 
      }
      System.out.print ("Error: enter number of times the course meets per week as an int "); //prompt to enter a positive integer
    }
    
    System.out.print ("Enter time class starts "); //prompt to enter a positive integer
    while (!myScanner.hasNextInt() || time<=0) { // while the input is invalid ie not a positive int
       if (myScanner.hasNextInt()){ // if it is an int
        time= myScanner.nextInt(); // set time to int
          if (time>0){ // if time is positive 
            break; // leave loop
          }
      }
      else { // if not an int
        String junkWord = myScanner.next(); // discard input 
      }
      System.out.print ("Error: enter time class starts as an int "); //prompt to enter a positive integer
    }

    System.out.print ("Enter an insturctor "); //prompt to name of prof
    while (myScanner.hasNextInt() || myScanner.hasNextDouble()) { // while the input is invalid ie a number
        String junkWord2 = myScanner.next(); // discard input
        System.out.print ("Error: enter an insturctor as an explicit string (no numbers) "); //prompt user to enter something that isnt a number
    }
    instructor = myScanner.next(); // set insturctor to string
    
    System.out.print ("Enter number of students "); //prompt to enter a positive integer
    while (!myScanner.hasNextInt() || student<=0) { // while the input is invalid ie not a positive int
       if (myScanner.hasNextInt()){ // if it is an int
        student= myScanner.nextInt(); // set student to int
          if (student>0){ // if student is positive 
             break; // leave loop
          }
      }
      else { // if not an int
        String junkWord = myScanner.next(); // discard input 
      }
      System.out.print ("Error: enter number of students as an int "); //prompt to enter a positive integer
    }
    
      } // end of main method
} //end of class