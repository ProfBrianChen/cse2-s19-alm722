//Amanda Morrison
//3-8-19
//CSE 02 section 210 hw06

// This program uses loops
// I will ask users for 4 integers and ensure correct type

import java.util.Scanner; // import a scanner

public class Network{
  // open class
 
  public static void main (String[] args) {
    //main method that you need for every java program 
 
    int height= 0; // define and initalize height
    int width= 0; //define and initalize width
    int size= 0; // define and initialize square size
    int edge= 0; //define and initialize edge length
    int count=0; //define and initialize counter
    int counter1 = 0; //define and initialize new counter
    
    Scanner myScanner = new Scanner(System.in); // making the instance of scanner declared which will take input
    
    System.out.print ("Enter integer for height "); //prompt to enter an integer for length
    while (!myScanner.hasNextInt() || height<=0) { // while the input is invalid ie not an int or 0 or negative
       if (myScanner.hasNextInt()){ // if it is an int
        height= myScanner.nextInt(); // set int to num
            if (height>0){ // if num is positive
              break; //leave loop
            }
      }
      else { // if not an int
        String junkWord = myScanner.next(); // discard input 
      }
      System.out.print ("Error: enter a positive int "); //prompt to enter a positive integer
    }
    
     System.out.print ("Enter integer for width "); //prompt to enter an integer for height
    while (!myScanner.hasNextInt() || width<=0) { // while the input is invalid ie not an int or 0 or negative
       if (myScanner.hasNextInt()){ // if it is an int
        width= myScanner.nextInt(); // set int to num
            if (width>0){ // if num is positive
              break; //leave loop
            }
      }
      else { // if not an int
        String junkWord = myScanner.next(); // discard input 
      }
      System.out.print ("Error: enter a positive int "); //prompt to enter a positive integer
    }
    
   System.out.print ("Enter integer for square size "); //prompt to enter an integer for square size 
    while (!myScanner.hasNextInt() || size<=0) { // while the input is invalid ie not an int or 0 or negative
       if (myScanner.hasNextInt()){ // if it is an int
        size= myScanner.nextInt(); // set int to num
            if (size>0){ // if num is positive
              break; //leave loop
            }
      }
      else { // if not an int
        String junkWord = myScanner.next(); // discard input 
      }
      System.out.print ("Error: enter a positive int "); //prompt to enter a positive integer
    }
    
   System.out.print ("Enter integer for edge length "); //prompt to enter an integer for edge length
    while (!myScanner.hasNextInt() || edge<=0) { // while the input is invalid ie not an int or 0 or negative
       if (myScanner.hasNextInt()){ // if it is an int
        edge= myScanner.nextInt(); // set int to num
            if (edge>0){ // if num is positive
              break; //leave loop
            }
      }
      else { // if not an int
        String junkWord = myScanner.next(); // discard input 
      }
      System.out.print ("Error: enter a positive int "); //prompt to enter a positive integer
    }

  
  if (size%2==0){ //if square size is even
    for(int i= 0; i < height; i++){ //run height number of times. rows
        if (i%(size-1) == 0 || i%(size+edge)==0){ //if one less than size goes into height perfectly
           for(int k= 0; k < ((width)/(size+edge)); k++){ //number of full iterations of the pattern 
             System.out.print ("#"); //print #
             for (int l = 0; l<size-2; l++){ // twice less than size to account for# on either side
                System.out.print ("-"); // print -
             } 
             System.out.print ("#"); //print #
             for (int j=0; j<edge; j++){ //for edge number of times
               System.out.print (" "); //print a space
             }
          }
          if (width%(size+height) != 0){ //if there is a remainder
            System.out.print ("#"); //print #
            for (int l=1; l<(width%(size+edge)); l++){ //for the remainder
              System.out.print("-"); //print -
            }
          }
        }else if (counter1 <size ){ //if one less than size doesnt go into height perfectly
          for (int m=0; m < ((width)/(size+edge)); m++){ //for the number of full iterations
            System.out.print ("|"); //print |
            if((counter1 % (size/2) == 0 && counter1 % size != 0)  ||  (((counter1+1) % (size/2) == 0 && counter1 % size != 0))){ //if the vertical line is half of the size but not at the inital position
             for (int b= 0; b<size-2; b++){ //twice less than size to account for # on either side
               System.out.print (" "); //print a space
            }
              System.out.print("|"); //print |
              for (int b= 0; b<edge; b++){ //for edge number of times
               System.out.print ("-"); //print -
            }
            }
            
            else{ //if verical line is not half of the size or starts at the initial position 
            for (int b= 0; b<size-2; b++){ //twice less than size to account for # on either side
               System.out.print (" "); //print a space
            }
              System.out.print ("|"); //print a |
              for (int f=0; f<edge; f++){ //for edge number of times
                System.out.print (" "); //print a space
              }
            }
            }
            if (width%(size+height) != 0){ //if there is a remainder
            System.out.print ("|"); //print |
              if ((width%(size+height)) ==(size/2)){ //if full iteration is equal to half of size 
                System.out.print (" "); //print a space
            }
            }
          }
      else{
     for (int m=0; m < (width/(size+edge)); m++){ //for the number of full iterations
        for (int q=0; q<(size/2)-1; q++){ //for one less than half of size
          System.out.print (" "); //print a space
        }
        System.out.print ("|"); //print |
        System.out.print ("|"); //print |
        for (int q=0; q<size/2-1; q++){ //for twice less than half of size 
          System.out.print (" "); //print a space
        }
        for (int q=0; q<edge; q++){ //for edge number of times
          System.out.print (" "); //print a space
        }
      }
        if (width%(size+height) != 0){ //if there is a remainder
          for(int q=0; q<width%(size+height); q++){ //for full iterations
            System.out.print (" "); //print |
          }
              if ((width%(size+height)) ==(size/2)){ // if full iteration is equal to half of size
                System.out.print ("|"); //print |
            }
            }
        
      }
          counter1++; //increase counter to signify new row
          System.out.println (""); //new line
      if (counter1> (size+edge)){ //if counter gets larger than one iteration
        counter1=0; //reset counter to 0
      }
        
    }

  }else{ // if square size is odd
      for(int i= 0; i < height; i++){ //number of colomns
        if (i%(edge+1) ==0){ //if no remainder/ partial loop to run thro
           for(int k= 0; k < (width/(edge+1)); k++){ //number of rows as an int, no need for remainer 
             System.out.print ("#"); //print #
             for (int l = 0; l<edge; l++){ // runs edge number of times
                System.out.print ("-"); // print -
             } 
          }
          if (width%(edge+1) != 0){ //if there is a remainer
            System.out.print ("#"); //print #
            for (int l=1; l<(width%(edge+1)); l++){ //for each of the remainders after one
              System.out.print("-"); // print -
            }
          }
          System.out.println (""); //start a new line
        }else{ // if there needs to be partials
          count= (width/(edge+1)); //defind count to be iterations of complete
          if (width%(edge+1) != 0){ //if there is a remainder
            count++; //increase count by one
          }
          for (int m=0; m<count; m++){ //run thro count number of times
            System.out.print ("|"); //pring |
            for (int b= 0; b<edge; b++){ //run through edge number of times
               System.out.print (" "); // print a space
            }
          }
          System.out.println (""); //new line
        }
      }
  }
      
      } // end of main method
} //end of class