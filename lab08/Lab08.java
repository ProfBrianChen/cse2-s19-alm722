//Amanda Morrison
//4-5-19
//CSE 02 section 210 hw07

// This program uses methods and arrays
// I will calculate the range, mean, and standard deviation 

import java.util.Scanner; // import a scanner
import java.util.Arrays; //import array
import java.util.Random; //import a random generator
import java.lang.Math; //import math

public class Lab08{
  // open class
  
  public static int getRange (int [] array, int times){ //get range method
    Arrays.sort(array); //sort the arrays in ascending order
    int range = array[times] - array[0]; //range is max- min
    return range; //return range
  }
  
  public static double getMean (int [] array, int times){ //get mean method
    int sum = 0; //sum starts at 0
    for (int i=0; i<times; i++){ //for each num in array
      sum = sum + array[i]; //add to the sum
    }
    double mean = sum / times; //mean is sum divided by numbers in array
    return mean; //return mean
  }
  
  public static double getStdDev (int [] array, int times, double mean){ //getStdDev method
    double sum = 0; //declare and initialize sum
    for (int i=0; i<times; i++){ //for each num in array
      sum = sum + array[i] - mean; //calculate the difference between num in array and mean and add that to sum
    }
    double stdDev = Math.sqrt (sum*2 / (times-1)); //calculate the standard deviation 
    return stdDev; //return the standard deviation 
  }
  
  public static void shuffle (int [] array, int times){ //shuffle method
    Random randomGenerator = new Random(); //making the instance of random declaration
 
    int rand1 = randomGenerator.nextInt(times); //make a random number
    int rand2 = randomGenerator.nextInt(times); //make a random number
    int temp = array[rand1]; //make temperary variable of array value
    int temp2 = array[rand2]; //make temperary variable of array value
    array[rand1] = temp2; //set array to temp
    array[rand2] = temp; //set array to temp
    
    for (int i = 0; i <times; i++){ //for each number in array
	array[i] = randomGenerator.nextInt (100); //random int
  System.out.println (array[i]); //print out array
}
  }
  
public static void main (String[] args) { 
    //main method that you need for every java program 
Scanner myScanner = new Scanner(System.in); // making the instance of scanner declared which will take input
Random randomGenerator = new Random(); //making the instance of random declaration
    
  
  int times = randomGenerator.nextInt(51) + 50; //random number between 50 and 100
  int [] array; //initialize array
  array = new int [times]; //set array length
  System.out.println (times); //print out array length
  
  for (int i = 0; i <times; i++){ //for each value in the array
	array[i] = randomGenerator.nextInt (100); //generate random int
  System.out.println (array[i]); //print out array value
}
  
  int range = getRange (array, times-1); //declare range 
  System.out.println ("range= " + range); //print range
  
  double mean = getMean (array, times-1); //declare mean
  System.out.println ("mean= " + mean); //print mean
  
  double stdDev = getStdDev (array, times-1, mean); //declare stdDev
  System.out.println ("standard deviation= " + stdDev); //print standard deviation

  shuffle (array, times-1); // run shuffle method 
  
   } // end of main method
} //end of class