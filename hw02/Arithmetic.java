//Amanda Morrison
//2-5-19
//CSE 02 section 210

// This program calculates the total cost of items bought including sales tax in various steps
// I will also ensure the values have only two decimal places

public class Arithmetic{
  //main method
  public static void main (String[] args){
    //input data
    
    //define variables
    int numPants=3; //number of pants
    double pantsPrice=34.98; //the cost of one pair of pants
    int numShirts=2; //number of shirts in cart
    double shirtPrice=24.99; //the cost of one shirt
    int numBelts=1; //the number of belts in cart
    double beltPrice=33.99; //the cost of one belt
    double paSalesTax=0.06; //tax rate in pa is 6%
    double totalCostOfPants; //declare variable for the total cost of pants
    double totalCostOfShirts; //declare variable for the total cost of shirts
    double totalCostOfBelts; //declare variable for the total cost of belts
    double SalesTaxPants; //declare variable for the sales tax on pants
    double SalesTaxShirts; //declare variable for the sales tax on shirts
    double SalesTaxBelts; //declare variable for the sales tax on belts
    double CostNoTax; //declare variable for the total cost of cart with no tax
    double CostTax; //declare variable for the total tax of the cart
    double CostTotal; //declare variable for the total cost of the cart with tax
    
    //calculate costs
    totalCostOfPants= (numPants*pantsPrice); // total cost of pants
    totalCostOfShirts= (numShirts*shirtPrice); //total cost of shirts
    totalCostOfBelts= (numBelts*beltPrice); //total cost of belts
    
    SalesTaxPants= (totalCostOfPants*paSalesTax); //sales tax charged on pants
    SalesTaxShirts= (totalCostOfShirts*paSalesTax); //sales tax charged on shirts
    SalesTaxBelts= (totalCostOfBelts*paSalesTax); //sales tax charged on belts
    
    //converting into two decimal places
    //ensure two decimal places for the sales tax on pants
    SalesTaxPants= SalesTaxPants*100; // move decimal two places to the right
    int totalSalesTaxPants= (int) SalesTaxPants; //convert into an int so no decimals
    double SalesTaxPantsf= (double) totalSalesTaxPants/100; //convery back into a double while dividing by 100 and moving the decimals two to the left
    
    //ensure two decimal places for the sales tax on shirts
    SalesTaxShirts= SalesTaxShirts*100; // move decimal two places to the right
    int totalSalesTaxShirts= (int) SalesTaxShirts; //convert into an int so no decimals
    double SalesTaxShirtsf= (double) totalSalesTaxShirts/100; //convery back into a double while dividing by 100 and moving the decimals two to the left
    
    //ensure two decimal places for the sales tax on belts
    SalesTaxBelts= SalesTaxBelts*100; // move decimal two places to the right
    int totalSalesTaxBelts= (int) SalesTaxBelts; //convert into an int so no decimals
    double SalesTaxBeltsf= (double) totalSalesTaxBelts/100; //convery back into a double while dividing by 100 and moving the decimals two to the left
    
    //calculate totals
    CostNoTax= totalCostOfPants+totalCostOfShirts+totalCostOfBelts; //total cost of purchase without tax
    CostTax= SalesTaxPantsf+ SalesTaxShirtsf+ SalesTaxBeltsf; //total sales tax
    CostTotal= CostNoTax+ CostTax; //total cost including sales tax
    
    
    //print statements
    System.out.println("The cost of pants without tax is "+ totalCostOfPants+ " and the tax charged is "+ SalesTaxPantsf); // print statement for total cost of pants and tax on pants
    System.out.println("The cost of shirts without tax is "+ totalCostOfShirts+ " and the tax charged is "+ SalesTaxShirtsf); // print statement for cost of shirts and the tax on it
    System.out.println("The cost of belts without tax is "+ totalCostOfBelts+ " and the tax charged is "+ SalesTaxBeltsf); // print statement for cost of belts and the tax on the cost of belts
    
    System.out.println("The total cost without tax is "+CostNoTax); //print statement for cost with no tax
    System.out.println("The total cost of tax is "+CostTax); //print statement for cost of tax
    System.out.println("The total cost with tax is "+CostTotal); //print statement for total cost with tax
    
  } // end of main method 
} //end of class