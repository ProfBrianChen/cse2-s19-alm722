//Amanda Morrison
//2-15-19
//CSE 02 section 210

// This program randomly selects 5 cards from a deck of cards and evaluates the poker hand
// I will generate a random number from 1 to 52

import java.lang.Math; // importing the math class

public class PokerHandCheck{
  // open class
  public static void main (String[] args) {
    //main method that you need for every java program 
    
     int myRandom1=(int)(Math.random()*52)+1; // create a random number with decimals between 0 and 1 not including 1. then multiply by 52 to have the range between 0 and 51, then cast to integer to make everything a whole number. then add one to have the range between 1 and 52
     int myRandom2=(int)(Math.random()*52)+1; // create a random number with decimals between 0 and 1 not including 1. then multiply by 52 to have the range between 0 and 51, then cast to integer to make everything a whole number. then add one to have the range between 1 and 52
     int myRandom3=(int)(Math.random()*52)+1; // create a random number with decimals between 0 and 1 not including 1. then multiply by 52 to have the range between 0 and 51, then cast to integer to make everything a whole number. then add one to have the range between 1 and 52
     int myRandom4=(int)(Math.random()*52)+1; // create a random number with decimals between 0 and 1 not including 1. then multiply by 52 to have the range between 0 and 51, then cast to integer to make everything a whole number. then add one to have the range between 1 and 52
     int myRandom5=(int)(Math.random()*52)+1; // create a random number with decimals between 0 and 1 not including 1. then multiply by 52 to have the range between 0 and 51, then cast to integer to make everything a whole number. then add one to have the range between 1 and 52
    
     String Card1=""; // initialize string varibale for the card
     String Suit1=""; // initialize string variable for the suit
     String Card2=""; // initialize string varibale for the card
     String Suit2=""; // initialize string variable for the suit
     String Card3=""; // initialize string varibale for the card
     String Suit3=""; // initialize string variable for the suit
     String Card4=""; // initialize string varibale for the card
     String Suit4=""; // initialize string variable for the suit
     String Card5=""; // initialize string varibale for the card
     String Suit5=""; // initialize string variable for the suit
    
    int number1= myRandom1%13; // find what number within a suit
    int number2= myRandom2%13; // find what number within a suit
    int number3= myRandom3%13; // find what number within a suit
    int number4= myRandom4%13; // find what number within a suit
    int number5= myRandom5%13; // find what number within a suit
    
    int numKing=0; //initialize number of that card
    int numQueen=0; //initialize number of that card
    int numJack=0;//initialize number of that card
    int num10=0; //initialize number of that card
    int num9=0;//initialize number of that card
    int num8=0;//initialize number of that card
    int num7=0;//initialize number of that card
    int num6=0;//initialize number of that card
    int num5=0;//initialize number of that card
    int num4=0;//initialize number of that card
    int num3=0;//initialize number of that card
    int num2=0;//initialize number of that card
    int numAce=0;//initialize number of that card
    
    int pair=0; //initialize number of pairs
    int three=0; // initialize number of three of a kinds
    
    
    if (myRandom1 <=13){ //if the card is less than 13 it is a diamond
      Suit1= "diamonds"; //for this case store in the string what suit it is
    }
    else if (myRandom1 >=14 && myRandom1 <=26){ //if the card is between these it is a club
      Suit1= "clubs"; //for this case store in the string what suit it is
    }
    else if (myRandom1 >=27 && myRandom1<=39){ //if the card is between the values it is a Heart
      Suit1= "hearts"; //for this case store in the string what suit it is
    }
    else if (myRandom1 > 40){ //if the card is greater than 40 it is a Spade
      Suit1= "spades"; //for this case store in the string what suit it is
    }
      
      switch (number1) { // switch statement to run through what card it is
        case 0: Card1= "the king of "; //for this case store the string what card it is
          numKing++; //incriment value by one
          break; // stop the switch statement
        case 1: Card1= "the ace of "; //for this case store the string what card it is
          numAce++; //incriment value by one
          break; // stop the switch statement
        case 2: Card1= "the 2 of "; //for this case store the string what card it is
          num2++; //incriment value by one
          break; // stop the switch statement
        case 3: Card1= "the 3 of "; //for this case store the string what card it is
          num3++; //incriment value by one
          break; // stop the switch statement
        case 4: Card1= "the 4 of "; //for this case store the string what card it is
          num4++; //incriment value by one
          break; // stop the switch statement
        case 5: Card1= "the 5 of "; //for this case store the string what card it is
          num5++; //incriment value by one
          break; // stop the switch statement
        case 6: Card1= "the 6 of "; //for this case store the string what card it is
          num6++; //incriment value by one
          break; // stop the switch statement
        case 7: Card1= "the 7 of "; //for this case store the string what card it is
          num7++; //incriment value by one
          break; // stop the switch statement
        case 8: Card1= "the 8 of "; //for this case store the string what card it is
          num8++; //incriment value by one
          break; // stop the switch statement
        case 9: Card1= "the 9 of "; //for this case store the string what card it is
          num9++; //incriment value by one
          break; // stop the switch statement
        case 10: Card1= "the 10 of "; //for this case store the string what card it is
          num10++; //incriment value by one
          break; // stop the switch statement
        case 11: Card1= "the jack of "; //for this case store the string what card it is
          numJack++; //incriment value by one
          break; // stop the switch statement
        case 12: Card1= "the queen of "; //for this case store the string what card it is
          numQueen++; //incriment value by one
          break; // stop the switch statement
      }
      
       if (myRandom2 <=13){ //if the card is less than 13 it is a diamond
      Suit2= "diamonds"; //for this case store in the string what suit it is
    }
    else if (myRandom2 >=14 && myRandom2 <=26){ //if the card is between these it is a club
      Suit2= "clubs"; //for this case store in the string what suit it is
    }
    else if (myRandom2 >=27 && myRandom2<=39){ //if the card is between the values it is a Heart
      Suit2= "hearts"; //for this case store in the string what suit it is
    }
    else if (myRandom2 > 40){ //if the card is greater than 40 it is a Spade
      Suit2= "spades"; //for this case store in the string what suit it is
    }
      
      switch (number2) { // switch statement to run through what card it is
        case 0: Card2= "the king of "; //for this case store the string what card it is
          numKing++; //incriment value by one
          break; // stop the switch statement
        case 1: Card2= "the ace of "; //for this case store the string what card it is
          numAce++; //incriment value by one
          break; // stop the switch statement
        case 2: Card2= "the 2 of "; //for this case store the string what card it is
          num2++; //incriment value by one
          break; // stop the switch statement
        case 3: Card2= "the 3 of "; //for this case store the string what card it is
          num3++; //incriment value by one
          break; // stop the switch statement
        case 4: Card2= "the 4 of "; //for this case store the string what card it is
          num4++; //incriment value by one
          break; // stop the switch statement
        case 5: Card2= "the 5 of "; //for this case store the string what card it is
          num5++; //incriment value by one
          break; // stop the switch statement
        case 6: Card2= "the 6 of "; //for this case store the string what card it is
          num6++; //incriment value by one
          break; // stop the switch statement
        case 7: Card2= "the 7 of "; //for this case store the string what card it is
          num7++; //incriment value by one
          break; // stop the switch statement
        case 8: Card2= "the 8 of "; //for this case store the string what card it is
          num8++; //incriment value by one
          break; // stop the switch statement
        case 9: Card2= "the 9 of "; //for this case store the string what card it is
          num9++; //incriment value by one
          break; // stop the switch statement
        case 10: Card2= "the 10 of "; //for this case store the string what card it is
          num10++; //incriment value by one
          break; // stop the switch statement
        case 11: Card2= "the jack of "; //for this case store the string what card it is
          numJack++; //incriment value by one
          break; // stop the switch statement
        case 12: Card2= "the queen of "; //for this case store the string what card it is
          numQueen++; //incriment value by one
          break; // stop the switch statement
      }
       if (myRandom3 <=13){ //if the card is less than 13 it is a diamond
      Suit3= "diamonds"; //for this case store in the string what suit it is
    }
    else if (myRandom3 >=14 && myRandom3 <=26){ //if the card is between these it is a club
      Suit3= "clubs"; //for this case store in the string what suit it is
    }
    else if (myRandom3 >=27 && myRandom3<=39){ //if the card is between the values it is a Heart
      Suit3= "hearts"; //for this case store in the string what suit it is
    }
    else if (myRandom3 > 40){ //if the card is greater than 40 it is a Spade
      Suit3= "spades"; //for this case store in the string what suit it is
    }
      
      switch (number3) { // switch statement to run through what card it is
        case 0: Card3= "the king of "; //for this case store the string what card it is
          numKing++; //incriment value by one
          break; // stop the switch statement
        case 1: Card3= "the ace of "; //for this case store the string what card it is
          numAce++; //incriment value by one
          break; // stop the switch statement
        case 2: Card3= "the 2 of "; //for this case store the string what card it is
          num2++; //incriment value by one
          break; // stop the switch statement
        case 3: Card3= "the 3 of "; //for this case store the string what card it is
          num3++; //incriment value by one
          break; // stop the switch statement
        case 4: Card3= "the 4 of "; //for this case store the string what card it is
          num4++; //incriment value by one
          break; // stop the switch statement
        case 5: Card3= "the 5 of "; //for this case store the string what card it is
          num5++; //incriment value by one
          break; // stop the switch statement
        case 6: Card3= "the 6 of "; //for this case store the string what card it is
          num6++; //incriment value by one
          break; // stop the switch statement
        case 7: Card3= "the 7 of "; //for this case store the string what card it is
          num7++; //incriment value by one
          break; // stop the switch statement
        case 8: Card3= "the 8 of "; //for this case store the string what card it is
          num8++; //incriment value by one
          break; // stop the switch statement
        case 9: Card3= "the 9 of "; //for this case store the string what card it is
          num9++; //incriment value by one
          break; // stop the switch statement
        case 10: Card3= "the 10 of "; //for this case store the string what card it is
          num10++; //incriment value by one
          break; // stop the switch statement
        case 11: Card3= "the jack of "; //for this case store the string what card it is
          numJack++; //incriment value by one
          break; // stop the switch statement
        case 12: Card3= "the queen of "; //for this case store the string what card it is
          numQueen++; //incriment value by one
          break; // stop the switch statement
      }
       if (myRandom4 <=13){ //if the card is less than 13 it is a diamond
      Suit4= "diamonds"; //for this case store in the string what suit it is
    }
    else if (myRandom4 >=14 && myRandom4 <=26){ //if the card is between these it is a club
      Suit4= "clubs"; //for this case store in the string what suit it is
    }
    else if (myRandom4 >=27 && myRandom4<=39){ //if the card is between the values it is a Heart
      Suit4= "hearts"; //for this case store in the string what suit it is
    }
    else if (myRandom4 > 40){ //if the card is greater than 40 it is a Spade
      Suit4= "spades"; //for this case store in the string what suit it is
    }
      
      switch (number4) { // switch statement to run through what card it is
        case 0: Card4= "the king of "; //for this case store the string what card it is
          numKing++; //incriment value by one
          break; // stop the switch statement
        case 1: Card4= "the ace of "; //for this case store the string what card it is
          numAce++; //incriment value by one
          break; // stop the switch statement
        case 2: Card4= "the 2 of "; //for this case store the string what card it is
          num2++; //incriment value by one
          break; // stop the switch statement
        case 3: Card4= "the 3 of "; //for this case store the string what card it is
          num3++; //incriment value by one
          break; // stop the switch statement
        case 4: Card4= "the 4 of "; //for this case store the string what card it is
          num4++; //incriment value by one
          break; // stop the switch statement
        case 5: Card4= "the 5 of "; //for this case store the string what card it is
          num5++; //incriment value by one
          break; // stop the switch statement
        case 6: Card4= "the 6 of "; //for this case store the string what card it is
          num6++; //incriment value by one
          break; // stop the switch statement
        case 7: Card4= "the 7 of "; //for this case store the string what card it is
          num7++; //incriment value by one
          break; // stop the switch statement
        case 8: Card4= "the 8 of "; //for this case store the string what card it is
          num8++; //incriment value by one
          break; // stop the switch statement
        case 9: Card4= "the 9 of "; //for this case store the string what card it is
          num9++; //incriment value by one
          break; // stop the switch statement
        case 10: Card4= "the 10 of "; //for this case store the string what card it is
          num10++; //incriment value by one
          break; // stop the switch statement
        case 11: Card4= "the jack of "; //for this case store the string what card it is
          numJack++; //incriment value by one
          break; // stop the switch statement
        case 12: Card4= "the queen of "; //for this case store the string what card it is
          numQueen++; //incriment value by one
          break; // stop the switch statement
      }
       if (myRandom5 <=13){ //if the card is less than 13 it is a diamond
      Suit5= "diamonds"; //for this case store in the string what suit it is
    }
    else if (myRandom5 >=14 && myRandom5 <=26){ //if the card is between these it is a club
      Suit5= "clubs"; //for this case store in the string what suit it is
    }
    else if (myRandom5 >=27 && myRandom5<=39){ //if the card is between the values it is a Heart
      Suit5= "hearts"; //for this case store in the string what suit it is
    }
    else if (myRandom5 > 40){ //if the card is greater than 40 it is a Spade
      Suit5= "spades"; //for this case store in the string what suit it is
    }
      
      switch (number5) { // switch statement to run through what card it is
        case 0: Card5= "the king of "; //for this case store the string what card it is
          numKing++; //incriment value by one
          break; // stop the switch statement
        case 1: Card5= "the ace of "; //for this case store the string what card it is
          numAce++; //incriment value by one
          break; // stop the switch statement
        case 2: Card5= "the 2 of "; //for this case store the string what card it is
          num2++; //incriment value by one
          break; // stop the switch statement
        case 3: Card5= "the 3 of "; //for this case store the string what card it is
          num3++; //incriment value by one
          break; // stop the switch statement
        case 4: Card5= "the 4 of "; //for this case store the string what card it is
          num4++; //incriment value by one
          break; // stop the switch statement
        case 5: Card5= "the 5 of "; //for this case store the string what card it is
          num5++; //incriment value by one
          break; // stop the switch statement
        case 6: Card5= "the 6 of "; //for this case store the string what card it is
          num6++; //incriment value by one
          break; // stop the switch statement
        case 7: Card5= "the 7 of "; //for this case store the string what card it is
          num7++; //incriment value by one
          break; // stop the switch statement
        case 8: Card5= "the 8 of "; //for this case store the string what card it is
          num8++; //incriment value by one
          break; // stop the switch statement
        case 9: Card5= "the 9 of "; //for this case store the string what card it is
          num9++; //incriment value by one
          break; // stop the switch statement
        case 10: Card5= "the 10 of "; //for this case store the string what card it is
          num10++; //incriment value by one
          break; // stop the switch statement
        case 11: Card5= "the jack of "; //for this case store the string what card it is
          numJack++; //incriment value by one
          break; // stop the switch statement
        case 12: Card5= "the queen of "; //for this case store the string what card it is
          numQueen++; //incriment value by one
          break; // stop the switch statement
      }
    
    System.out.println ("Your random cards were:"); //print statement
    System.out.println (Card1 + Suit1); // print statement 
    System.out.println (Card2 + Suit2); // print statement
    System.out.println (Card3 + Suit3); // print statement
    System.out.println (Card4 + Suit4); // print statement
    System.out.println (Card5 + Suit5); // print statement
    
    if (numKing ==2){ //evaluate if you have a pair 
      pair++;//incriment value by one
    }
     if (numQueen ==2){ //evaluate if you have a pair 
      pair++;//incriment value by one
    }
     if (numJack ==2){ //evaluate if you have a pair 
      pair++;//incriment value by one
    }
     if (num10 ==2){ //evaluate if you have a pair 
      pair++;//incriment value by one
    }
     if (num9 ==2){ //evaluate if you have a pair 
      pair++;//incriment value by one
    }
     if (num8  ==2){ //evaluate if you have a pair 
      pair++;//incriment value by one
    }
     if (num7 ==2){ //evaluate if you have a pair 
      pair++;//incriment value by one
    }
     if (num6 ==2){ //evaluate if you have a pair 
      pair++;//incriment value by one
    }
     if (num5 ==2){ //evaluate if you have a pair 
      pair++;//incriment value by one
    }
     if (num4 ==2){ //evaluate if you have a pair 
      pair++;//incriment value by one
    }
     if (num3 ==2){ //evaluate if you have a pair 
      pair++;//incriment value by one
    }
     if (num2 ==2){ //evaluate if you have a pair 
      pair++;//incriment value by one
    }
     if (numAce ==2){ //evaluate if you have a pair 
      pair++;//incriment value by one
    }
    
    
    if (numKing ==3){ //evaluate if you have three of a kind 
      three++;//incriment value by one
    }
     if (numQueen ==3){ //evaluate if you have three of a kind 
      three++;//incriment value by one
    }
     if (numJack ==3){//evaluate if you have three of a kind 
      three++;//incriment value by one
    }
     if (num10 ==3){//evaluate if you have three of a kind 
      three++;//incriment value by one
    }
     if (num9 ==3){//evaluate if you have three of a kind 
      three++;//incriment value by one
    }
     if (num8  ==3){ //evaluate if you have three of a kind 
      three++;//incriment value by one
    }
     if (num7 ==3){ //evaluate if you have three of a kind 
      three++;//incriment value by one
    }
    
     if (num6 ==3){ //evaluate if you have three of a kind 
      three++;//incriment value by one
    }
     if (num5 ==3){ //evaluate if you have three of a kind 
      three++;//incriment value by one
    }
     if (num4 ==3){ //evaluate if you have three of a kind 
      three++;//incriment value by one
    }
     if (num3 ==3){ //evaluate if you have three of a kind 
      three++;//incriment value by one
    }
     if (num2 ==3){ //evaluate if you have three of a kind 
      three++;//incriment value by one
    }
     if (numAce ==3){ //evaluate if you have three of a kind 
      three++;//incriment value by one
    }
      
    
    
    if (pair==1){ // if you have one pair
      System.out.println ("\nYou have a pair!"); //print statment 
    }
    else if (pair==2){ // if you have two pairs
      System.out.println ("\nYou have two pairs!");//print statment 
    }
    else if (three>0){ // if you have three of a kind
      System.out.println ("\nYou have three of a kind!");//print statment 
    }
    else{ // if you have nothing
      System.out.println("\nYou have a high card hand!");//print statment 
    }
      
   } // end of main method 
} //end of class